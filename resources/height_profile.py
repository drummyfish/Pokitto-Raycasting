# Convert png image of height profile to number array.
#
# author: Miloslav Ciz
# license: CC0

import sys
from PIL import Image

image = Image.open(sys.argv[1])
pixels = image.load()

sys.stdout.write("const int8_t heightProfile[] = {")

first = True

count = 0;

for x in range(image.size[0]):
  if count % 80 == 0:
    print("")

  count += 1

  if first:
    first = False
  else:
    sys.stdout.write(",")

  for y in range(image.size[1]):
    p = pixels[x,y]

    if p[0] == 0:
      sys.stdout.write(str(image.size[1] - y))
      break

print("};")

